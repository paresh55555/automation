package page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.ets.automation.Driver;

public class SampleObjects {

	
	@FindBy(xpath = "//input[@name='firstname']")
	public static WebElement txtFirstName;
	
	@FindBy(xpath = "//input[@name='lastname']")
	public static WebElement txtLastName;
	
	@FindBy(xpath = "//input[@aria-label='Mobile number or email address']")
	public static WebElement txtMobileOrEmail;
	
	@FindBy(xpath = "//input[@data-type='password']")
	public static WebElement txtPassword;
	
	@FindBy(xpath = "//input[@value='2']")
	public static WebElement rdMale;
	
	@FindBy(xpath = "//button[@name='websubmit']")
	public static WebElement btnSignUp;
	
	@FindBy(xpath = "//select[@id='day']")
	public static WebElement daySelect;
	
	@FindBy(xpath = "//select[@title='Month']")
	public static WebElement monthSelect;
	
	@FindBy(xpath = "//select[@title='Year']")
	public static WebElement yearSelect;
	
	@FindBy(xpath = "//div[text()='Create a new account']")
	public static WebElement lblCreateNewAccount;
	
	static {

		PageFactory.initElements(Driver.driver, SampleObjects.class);
	}
}
