package page.actions;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ets.automation.BasePageActions;
import com.ets.automation.Database;

import page.objects.SampleObjects;

public class SampleActions extends BasePageActions {

	
	public static void verifyWithDatabase()  {
		
		
		
	}
	
	
	
	public static void facebookName_action() {

		clear(SampleObjects.txtFirstName, "clear firstname");
		sendkeys(SampleObjects.txtFirstName, "Ashish1", "writing first name");
		waiting(200);
		sendkeys(SampleObjects.txtLastName, "Davda1", "writing last name");
		waiting(200);
	}

	public static void facebookMobileNumber_action() {

		clear(SampleObjects.txtMobileOrEmail, "clear txtMobileOrEmail");
		sendkeys(SampleObjects.txtMobileOrEmail, "992056788", "writing MobileOrEmail");
		waiting(200);
	}

	public static void facebookSPassword_action() {

		sendkeys(SampleObjects.txtPassword, "neet@1234", "writing password");
		waiting(200);
	}

	public static void facebookSelectGender_action() {

		click(SampleObjects.rdMale, "selecting male radio");
	}

	public static void facebookSelectBirthDate_action() {

		selectByIndexDropDown(SampleObjects.daySelect, 2, "selecting day");
		waiting(1000);
		selectByIndexDropDown(SampleObjects.monthSelect, 3, "selecting Month");
		waiting(1000);
		selectByValueDropDown(SampleObjects.yearSelect, "2000", "selecting Year");
		waiting(1000);
	}

	public static void facebookSignUp_action() {

		click(SampleObjects.btnSignUp, "click on signup");
	}

}
