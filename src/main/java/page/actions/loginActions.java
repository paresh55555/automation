package page.actions;

import com.ets.automation.BasePageActions;

import page.objects.loginObjects;

public class loginActions extends BasePageActions{
	
	
	public static void Login_action() {
		
		click(loginObjects.login, "login click");
		sendkeys(loginObjects.emailId, "admin@facelab.com.au", "email check");
		sendkeys(loginObjects.password, "hiren123", "Password check");
		click(loginObjects.btnSubmit, "press submit button");
		waiting(10000);
		//click(loginObjects.mypatient,"Clicking mypatient  ");
		//waiting(4000);
	}
	
	public static void Add_patient() {
		
		click(loginObjects.addPatient, "click patient btn");
		click(loginObjects.clickTitle, "click title input");
		waiting(4000);
		click(loginObjects.selectTitle, "Select title name");
		waiting(2000);
		sendkeys(loginObjects.firstName, "Alex", "Enter the First Name");
		sendkeys(loginObjects.surName,"John", "Enter the SurName"); 
		waiting(4000);
		sendkeys(loginObjects.preferredName, "Alex", "Enter the preferred Name");
		waiting(4000);
		click(loginObjects.monthClick, "Open the Month dropdown");
		click(loginObjects.selectMonth, "Select Month from dropdown");
		waiting(2000);
		click(loginObjects.dayClick, "Open the Day dropdown");
		waiting(2000);
		click(loginObjects.selectDay, "Select Day from dropdown");
		waiting(2000);
		click(loginObjects.yearClick, "Open the Year dropdown");
		waiting(2000);
		click(loginObjects.selectYear, "Select Year from dropdown");
		waiting(2000);
		sendkeys(loginObjects.selectAddress,"Sydney Road", "Enter the address Detail");
		waiting(2000);
		sendkeys(loginObjects.suburb,"Campbellfield", "Enter the Suburb Detail");
		waiting(2000);
		sendkeys(loginObjects.states,"Victoria", "Enter the state Detail");
		waiting(2000);
		sendkeys(loginObjects.postcode,"3061", "Enter the Postcode Detail");
		waiting(2000);
		sendkeys(loginObjects.country,"Australia", "Enter the Country Detail");
		waiting(2000);
		sendkeys(loginObjects.ocuupation,"Business", "Enter the Business Detail");
		waiting(2000);
		sendkeys(loginObjects.businessFirm,"Corporation", "Enter the firm Detail");
		waiting(2000);
		click(loginObjects.btnSubmit, "press submit button");
		sendkeys(loginObjects.mobileNo,"8898785455", "Enter the Tel number");
		waiting(1500);
	    sendkeys(loginObjects.patientEmail,"alex@gmail.com", "Enter the paitent email id");
		waiting(1500);		
		click(loginObjects.hospitalCover,"open the dropdown for hospital cover");
		waiting(1500);
		click(loginObjects.selectHospitalcoverOption,"select the option ");
		waiting(1500);
		
                                                              
	}
                
}
     